resources = [
    {
        "name": "details",
        "keywords": ("details",),
        "phrase": ("details about INeed for Siloam Springs",),
        "options": ("Which info do you want?",),
        "children": (
            {
                "name": "About",
                "keywords": ("1 About",),
                "info": ({
                    "Summary": "INeed is a directory created and run by Peter Granderson.",
                    "Website": "https://ineedsiloam.com",
                },),
                "isLeaf": True,
                "phrase": ("1.About",),
            },
            {
                "name": "Terms of Use",
                "keywords": ("2 Terms",),
                "info": ({
                    "Summary": "INeed is only an automated info directory. It is provided as-is, without guarantee. For full terms, see:",
                    "Website": "https://bit.ly/2WvAtUK",
                },),
                "isLeaf": True,
                "phrase": ("2.Terms of Use",),
            },
            {
                "name": "Privacy Policy",
                "keywords": ("3 Privacy",),
                "info": ({
                    "Summary": "We do not store what you type, and no human ever sees it. We track very little. For full policy, see:",
                    "Website": "https://bit.ly/2KdHq68",
                },),
                "isLeaf": True,
                "phrase": ("3.Privacy Policy",),
            },
        )
    },
    {
        "name": "dental",
        "keywords": ("teeth tooth mouth cavity dentures extractions extracted extract dental dentistry",),
        "phrase": ("a low-cost dental clinic",),
        "options": ("Which dental clinic do you want info about?",),
        "children": (
            {
                "name": "Samaritan Dental Clinic",
                "keywords": ("1 Samaritan",),
                "info": ({
                    "Phone": "479-636-0451",
                    "Hours": "8:30-3:30, M-Th",
                    "Location": "Rogers",
                    "Notes": "Appointment required",
                },),
                "isLeaf": True,
                "phrase": ("1.Samaritan Dental",),
            },
            {
                "name": "Community Clinic Dental",
                "keywords": ("2 Community",),
                "info": ({
                    "Phone": "479-636-0451",
                    "Hours": "8-5 M-Th, 8-1 F",
                    "Address": "3710 W Southern Hills Blvd #700, Rogers",
                    "Notes": "Appointment required",
                },),
                "isLeaf": True,
                "phrase": ("2.Community Clinic",),
            },
        )
    },
    {
        "name": "clinic",
        "keywords": (
            "clinic doctor nurse practitioner sick ill illness vomiting pain flu influenza injury injured wounded",
        ),
        "phrase": ("a medical clinic",),
        "options": ("What kind of clinic are you needing?",),
        "children": (
            {
                "name": "urgent-care",
                "keywords": ("urgent-care urgent walk-in",),
                "phrase": ("an urgent-care clinic",),
                "children": [
                    {
                        "name": "northwest urgent-care",
                        "keywords": ("1 Northwest",),
                        "info": ({
                            "Address": "3271 E US 412 Hwy",
                            "Phone": "479-215-3080",
                            "Location": "Near Lowes on 412",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Northwest Urgent Care",),
                    }
                ]
            },
            {
                "name": "primary-care",
                "keywords": ("primary-care primary family-care family-practice",),
                "phrase": ("a primary-care clinic",),
                "children": [
                    {
                        "name": "Community Clinic",
                        "keywords": ("1 Community",),
                        "info": ({
                            "For": "Kids + Adults",
                            "Phone": "855-438-2280",
                            "Location": "Across from library",
                            "Address": "500 S Mt Olive",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Community Clinic",),
                    },
                    {
                        "name": "Panther Clinic",
                        "keywords": ("2 Panther",),
                        "info": ({
                            "For": "Kids + Adults",
                            "Phone": "479-524-8175",
                            "Address": "1500 N Mt Olive",
                            "Location": "In Intermediate School",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Panther Clinic",),
                    },
                ]
            },
            {
                "name": "STD screening",
                "keywords": ("sexually transmitted sti std",),
                "phrase": ("an STD screening clinic",),
                "children": [
                    {
                        "name": "Public Health",
                        "keywords": ("1 Public",),
                        "info": ({
                            "Phone": "479-549-3790",
                            "Address": "101 W University St",
                            "Location": "Downtown Siloam",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Public Health",),
                    },
                    {
                        "name": "Community Clinic",
                        "keywords": ("2 Community",),
                        "info": ({
                            "For": "Kids + Adults",
                            "Phone": "855-438-2280",
                            "Location": "Across from library",
                            "Address": "500 S Mt Olive",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Community Clinic",),
                    },
                    {
                        "name": "Panther Clinic",
                        "keywords": ("3 Panther",),
                        "info": ({
                            "For": "Kids + Adults",
                            "Phone": "479-524-8175",
                            "Address": "1500 N Mt Olive",
                            "Location": "In Intermediate School",
                        },),
                        "isLeaf": True,
                        "phrase": ("3.Panther Clinic",),
                    },
                ]
            },
            {
                "name": "pediatric-care",
                "keywords": ("pediatrician pediatric child",),
                "phrase": ("a children's clinic",),
                "children": [
                    {
                        "name": "Panther Clinic",
                        "keywords": ("1 Panther",),
                        "info": ({
                            "For": "Children + Adults",
                            "Phone": "479-524-8175",
                            "Address": "1500 N Mt Olive",
                            "Location": "In Intermediate School",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Panther Clinic",),
                    },
                    {
                        "name": "Sager Creek",
                        "keywords": ("2 Sager Creek",),
                        "info": ({
                             "For": "Children",
                             "Phone": "479-549-4228",
                             "Address": "1101-2 N Progress Ave",
                             "Location": "Near hospital",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Sager Creek",),
                    }
                ]
            },
            {
                "name": "vaccines",
                "keywords": ("vaccine vaccination immunization immunize shot",),
                "phrase": ("a vaccination clinic",),
                "children": [
                    {
                        "name": "Public Health",
                        "keywords": ("1 Public",),
                        "info": ({
                            "Phone": "479-549-3790",
                            "Address": "101 W University St",
                            "Location": "Downtown Siloam",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Public Health",),
                    },
                    {
                        "name": "Panther Clinic",
                        "keywords": ("2 Panther",),
                        "info": ({
                            "Phone": "479-524-8175",
                            "Address": "1500 N Mt Olive",
                            "Location": "In Intermediate School",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Panther Clinic",),
                    },
                    {
                        "name": "Community Clinic",
                        "keywords": ("3 Community",),
                        "info": ({
                            "Phone": "855-438-2280",
                            "Location": "Across from library",
                            "Address": "500 S Mt Olive",
                        },),
                        "isLeaf": True,
                        "phrase": ("3.Community Clinic",),
                    },
                ],
            },
        )
    },
    {
        "name": "food",
        "keywords": ("food hungry hunger eat eating ate",),
        "phrase": ("a food service",),
        "options": ("What kind of food help do you need?",),
        "children": (
            {
                "name": "food for children",
                "keywords": ("children",),
                "phrase": ("food for children",),
                "children": [
                    {
                        "name": "WIC",
                        "keywords": ("1 WIC",),
                        "info": ({
                            "Phone": "479-549-3790",
                            "Address": "101 W University St",
                            "Location": "Downtown Siloam",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.WIC",),
                    },
                    {
                        "name": "Food Stamps/SNAP",
                        "keywords": ("2 stamps SNAP",),
                        "info": ({
                            "Phone": "501-682-8650",
                            "Website": "https://bit.ly/2HLhld4",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Food Stamps/SNAP",),
                    },
                    {
                        "name": "Summer Food Program",
                        "keywords": ("3 summer",),
                        "info": ({
                            "Locations": "Allen Elementary 11-1, Bob Henry Park 11:30",
                            "Website": "https://bit.ly/1pZHu5w",
                        },),
                        "isLeaf": True,
                        "phrase": ("3.Summer Food Program",),
                    },
                ]
            },
            {
                "name": "free meal",
                "keywords": ("meal",),
                "phrase": ("a free meal",),
                "children": [
                    {
                        "name": "Genesis House",
                        "keywords": ("1 Genesis",),
                        "info": ({
                            "Phone": "479-549-3438",
                            "Address": "1402 N Inglewood",
                            "Locations": "Across from Dollar General on Cheri Whitlock",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Genesis House",),
                    },
                ]
            },
            {
                "name": "food pantry",
                "keywords": ("pantry locker bank",),
                "phrase": ("a food pantry",),
                "children": [
                    {
                        "name": "Manna Center",
                        "keywords": ("1 Manna Center",),
                        "info": ({"address": "670 Heritage Ct.", "near": "The intersection of S. Carl and W. Tulsa"},),
                        "isLeaf": True,
                        "phrase": ("1.The Manna Center",),
                    },
                    {
                        "name": "Only Believe",
                        "keywords": ("2 Tabernacle",),
                        "info": ({
                            "Phone": "479-790-3678",
                            "Address": "1700 A South Pointe",
                            "Location": "Off of 59 south of 412",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Only Believe Tabernacle",),
                    },
                    {
                        "name": "Hunger and Thirst Ministries",
                        "keywords": ("3 thirst ministry",),
                        "info": ({
                            "Phone": "479-373-6220",
                            "Address": "3298 E Kenwood St",
                            "Location": "Off of 16 south of 412",
                        },),
                        "isLeaf": True,
                        "phrase": ("3.Hunger & Thirst",),
                    }
                ]
            },
        )
    },
    {
        "name": "housing",
        "keywords": ("house housing homeless apartment flat evict evicted evicting place room sleep",),
        "phrase": ("housing assistance",),
        "options": ("What kind of housing help do you need?",),
        "children": (
            {
                "name": "rent/hotel assistance",
                "keywords": ("house housing rent renting hotel motel over-night",),
                "phrase": ("rent/hotel assistance",),
                "children": [
                    {
                        "name": "Housing Authority",
                        "keywords": ("1 authority",),
                        "info": ({
                            "Phone": "479-524-8117",
                            "Address": "1255 W Tulsa",
                            "Locations": "Near Middle School",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Housing Authority",),
                    },
                    {
                        "name": "Genesis House",
                        "keywords": ("2 Genesis",),
                        "info": ({
                            "Phone": "479-549-3438",
                            "Address": "1402 N Inglewood",
                            "Locations": "Across from Dollar General on Cheri Whitlock",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Genesis House",),
                    },
                ]
            },
            {
                "name": "shelter",
                "keywords": ("shelter temporary",),
                "phrase": ("a shelter",),
                "children": [
                    {
                        "name": "general shelter",
                        "keywords": ("general",),
                        "phrase": ("a general shelter",),
                        "children": [
                            {
                                "name": "Genesis House",
                                "keywords": ("1 Genesis",),
                                "info": ({
                                    "Phone": "479-549-3438",
                                    "Services": "Day-shelter, laundry, shower",
                                    "Address": "1402 N Inglewood",
                                    "Locations": "Across from Dollar General on Cheri Whitlock",
                                },),
                                "isLeaf": True,
                                "phrase": ("1.Genesis House",),
                            },
                            {
                                "name": "Salvation Army",
                                "keywords": ("2 Salvation Army",),
                                "info": ({
                                    "Phone": "855-251-0857",
                                    "Location": "Bentonville, Fayetteville",
                                },),
                                "isLeaf": True,
                                "phrase": ("2.Salvation Army",),
                            },
                            {
                                "name": "Havenwood",
                                "keywords": ("3 Havenwood",),
                                "info": ({
                                    "Phone": "479-273-1060",
                                    "For": "single parents",
                                    "Location": "Bentonville",
                                },),
                                "isLeaf": True,
                                "phrase": ("3.Havenwood",),
                            },
                        ]
                    },
                    {
                        "name": "childrens shelter",
                        "keywords": ("child children boy girl youth baby",),
                        "phrase": ("children's shelter",),
                        "children": [
                            {
                                "name": "NWA Children's Shelter",
                                "keywords": ("1 NWA",),
                                "info": ({
                                    "Phone": "479-795-2417",
                                    "Location": "Bentonville",
                                },),
                                "isLeaf": True,
                                "phrase": ("1.NWA Children's Shelter",),
                            },
                            {
                                "name": "EOA Children's House",
                                "keywords": ("2 EOA",),
                                "info": ({
                                    "Phone": "479-927-1232",
                                    "Location": "Springdale",
                                },),
                                "isLeaf": True,
                                "phrase": ("2.EOA Children's House",),
                            },
                            {
                                "name": "YouthBridge",
                                "keywords": ("3 Bridge",),
                                "info": ({
                                    "Phone": "479-521-1532",
                                    "Location": "Fayetteville",
                                },),
                                "isLeaf": True,
                                "phrase": ("3.Youth Bridge",),
                            },
                        ]
                    },
                    {
                        "name": "womens shelter",
                        "keywords": ("women woman ladies lady",),
                        "phrase": ("women's shelter",),
                        "children": [
                            {
                                "name": "NW Womens Shelter",
                                "keywords": ("1 NW northwest",),
                                "info": ({
                                    "Phone": "800-775-9011",
                                },),
                                "isLeaf": True,
                                "phrase": ("1.NW Women's Shelter",),
                            },
                            {
                                "name": "Restoration Village",
                                "keywords": ("1 Restoration Restore",),
                                "info": ({
                                    "Phone": "479-631-7345",
                                    "For": "women & children",
                                    "Location": "Little Flock",
                                },),
                                "isLeaf": True,
                                "phrase": ("1.Restoration Village",),
                            },
                            {
                                "name": "Oasis of NWA",
                                "keywords": ("2 NW northwest",),
                                "info": ({
                                    "Phone": "479-268-4340",
                                    "For": "women & children",
                                    "Location": "Bentonville",
                                },),
                                "isLeaf": True,
                                "phrase": ("2.Oasis of NWA",),
                            },
                            {
                                "name": "Hannah House",
                                "keywords": ("3 Hannah",),
                                "info": ({
                                    "Phone": "479-782-5683",
                                    "For": "young women",
                                    "Location": "Fort Smith",
                                },),
                                "isLeaf": True,
                                "phrase": ("3.Hannah House",),
                            },
                        ]
                    },
                    {
                        "name": "mens shelter",
                        "keywords": ("men man",),
                        "phrase": ("men's shelter",),
                        "children": [
                            {
                                "name": "Souls Harbor",
                                "keywords": ("1 Soul Harbor",),
                                "info": ({
                                    "Phone": "479-631-7878",
                                },),
                                "isLeaf": True,
                                "phrase": ("1.Soul's Harbor",),
                            },
                        ]
                    },
                ]
            },
        )
    },
    {
        "name": "clothing",
        "keywords": ("clothes clothing shirt pants shoe sock coat jacket hat underwear panties bra",),
        "phrase": ("help with clothing",),
        "options": ("What kind are you looking for?",),
        "children": (
            {
                "name": "child clothing",
                "keywords": ("child children boy girl youth baby infant toddler",),
                "phrase": ("child clothing",),
                "children": [
                    {
                        "name": "BrightFutures",
                        "keywords": ("1 Bright Futures Future",),
                        "info": ({
                            "Phone": "479-524-8175",
                            "Address": "1500 N Mt Olive",
                            "Location": "In Intermediate School",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Bright Futures",),
                    },
                    {
                        "name": "Potter's House",
                        "keywords": ("2 Potter",),
                        "info": ({
                                    "Phone": "479-373-6373",
                                    "Address": "2101 US-412",
                                    "Location": "Across from 2B's Auto",
                                    "For": "all ages"
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Potter's House",),
                    },
                ]
            },
            {
                "name": "adult clothing",
                "keywords": ("adult man woman lady old",),
                "phrase": ("adult clothing",),
                "children": [
                    {
                        "name": "Potter's House",
                        "keywords": ("1 Potter",),
                        "info": ({
                                     "Phone": "479-373-6373",
                                     "Address": "2101 US-412",
                                     "Location": "Across from 2B's Auto",
                                 },),
                        "isLeaf": True,
                        "phrase": ("1.Potter's House",),
                    },
                ]
            },
        )
    },
    {
        "name": "smoking cessation",
        "keywords": ("smoking smoke smokes smoker vape vaping vaper vapes tobacco",),
        "phrase": ("help stopping tobacco",),
        "options": ("What help with tobacco do you need?",),
        "children": (
            {
                "name": "Tobacco Helpline",
                "keywords": ("1 Helpline",),
                "info": ({
                    "Phone": "833-283-9355",
                },),
                "isLeaf": True,
                "phrase": ("1.Helpline",),
            },
            {
                "name": "SmokefreeTXT",
                "keywords": ("2 Text",),
                "info": ({
                     "Phone": "Text 'Arkansas' 47848",
                },),
                "isLeaf": True,
                "phrase": ("2.Text Program",),
            },
        )
    },
    {
        "name": "transporation",
        "keywords": ("taxi taxies ride lift transport transportation transported van car bus buses uber",),
        "phrase": ("help with transportation",),
        "options": ("Which option do you want info about?",),
        "children": (
            {
                "name": "Siloam Springs Taxi Program",
                "keywords": ("1 Siloam Springs Sprgs program",),
                "info": ({
                    "Phone": "479-373-1811",
                },),
                "isLeaf": True,
                "phrase": ("1.Siloam Spgs Taxi Program",),
            },
            {
                "name": "Medicaid Transport",
                "keywords": ("2 Medicaid",),
                "info": ({
                     "Phone": "1-888-833-4136",
                },),
                "isLeaf": True,
                "phrase": ("2.Medicaid Transport",),
            },
        )
    },
    {
        "name": "deaf resources",
        "keywords": ("deaf hard hearing sign mute speech",),
        "phrase": ("help with resources for the deaf",),
        "options": ("Which resource do you want info about?",),
        "children": (
            {
                "name": "Arkansas Rehabilitation Services",
                "keywords": ("1 Rehabilitation",),
                "info": ({
                    "Phone": "479-582-1286",
                    "Services": "Training & career preparation",
                    "Location": "Fayetteville",
                    "Website": "https://bit.ly/2M97mCP",
                },),
                "isLeaf": True,
                "phrase": ("1.AR Deaf Career Center",),
            },
            {
                "name": "Arkansas Deaf Career Center",
                "keywords": ("2 Career",),
                "info": ({
                    "Phone": "501-231-1385",
                    "Services": "School for the deaf for ages K-21",
                    "Location": "Little Rock",
                    "Website": "https://bit.ly/2YKoUGD",
                },),
                "isLeaf": True,
                "phrase": ("2.AR Deaf Career Center",),
            },
            {
                "name": "NW Arkansas Assoc. of the Deaf",
                "keywords": ("3 NW Arkansas Assoc Association Deaf",),
                "info": ({
                    "Phone": "479-439-8316",
                    "Website": "https://www.nwaad.com",
                },),
                "isLeaf": True,
                "phrase": ("3.NW Arkansas Assoc. of the Deaf",),
            },
            {
                "name": "AbilityTree",
                "keywords": ("4 Ability Tree AbilityTree",),
                "info": ({
                    "Phone": "855-288-6735",
                    "Services": "Education/support for people/family-members with disabilities",
                    "Address": "300 E Main St",
                    "Location": "Siloam Springs old downtown",
                    "Website": "https://bit.ly/2HAnxVl",
                },),
                "isLeaf": True,
                "phrase": ("4.NW Arkansas Assoc. of the Deaf",),
            },
        )
    },
    {
        "name": "glasses for children",
        "keywords": ("glasses eyeglasses vision",),
        "phrase": ("help with getting glasses for children",),
        "options": ("Which glasses resource for children do you want?",),
        "children": (
            {
                "name": "Siloam Springs School District",
                "keywords": ("1 Siloam Springs Schools",),
                "info": ({
                    "Phone": "479-524-8175",
                    "Services": "May be able to assist school children in getting glasses",
                },),
                "isLeaf": True,
                "phrase": ("1.Siloam Springs Schools",),
            },
        )
    },
    {
        "name": "blind resources",
        "keywords": ("blind sight braille",),
        "phrase": ("help with resources for the blind",),
        "options": ("Which resource would you like info about?",),
        "children": (
            {
                "name": "AR Division of Services for the Blind",
                "keywords": ("1 Division Blind",),
                "info": ({
                    "Phone": "479-527-2580",
                    "Address": "4044 North Frontage Road, Fayetteville",
                    "Services": "Youth and elderly programs; assistance with school/jobs",
                    "Website": "https://bit.ly/2Wm97jF",
                },),
                "isLeaf": True,
                "phrase": ("1.AR Division of Services for the Blind",),
            },
            {
                "name": "AR Talking Books",
                "keywords": ("2 Talking Books",),
                "info": ({
                    "Phone": "501-682-2053",
                    "Notes": "Run by AR State Library, available by distance",
                    "Website": "https://bit.ly/2JX4BBM",
                },),
                "isLeaf": True,
                "phrase": ("2.AR Talking Books",),
            },
        )
    },
    {
        "name": "utilities",
        "keywords": ("utility utilities water electric electricity sewage garbage bill",),
        "phrase": ("help with utility bills",),
        "options": ("The following may be able to help. Which do you want?",),
        "children": (
            {
                "name": "Office of Human Concern",
                "keywords": ("1 Office Human Concern",),
                "info": ({
                    "Phone": "479-636-7301",
                },),
                "isLeaf": True,
                "phrase": ("1.Office of Human Concern",),
            },
            {
                "name": "Manna Center",
                "keywords": ("2 Manna Center",),
                "info": ({"address": "670 Heritage Ct.", "near": "The intersection of S. Carl and W. Tulsa"},),
                "isLeaf": True,
                "phrase": ("2.The Manna Center",),
            },
            {
                "name": "Salvation Army",
                "keywords": ("3 Salvation Army",),
                "info": ({
                    "phone": "479-751-2077", 
                    "website": "https://bit.ly/30C8amC"
                },),
                "isLeaf": True,
                "phrase": ("3.Salvation Army",),
            },
        )
    },
    {
        "name": "shower",
        "keywords": ("shower showering showered bath bathe bathing",),
        "phrase": ("help getting a free shower",),
        "options": ("Here is a place that may be able to help:",),
        "children": (
            {
                "name": "Genesis House",
                "keywords": ("1 Genesis",),
                "info": ({
                    "Phone": "479-549-3438",
                    "Address": "1402 N Inglewood",
                    "Locations": "Across from Dollar General on Cheri Whitlock",
                },),
                "isLeaf": True,
                "phrase": ("1.Genesis House",),
            },
        )
    },
    {
        "name": "laundry",
        "keywords": ("laundry laundering wash washing laundromat",),
        "phrase": ("help with washing clothes",),
        "options": ("Here are some options that may help with laundry:",),
        "children": (
            {
                "name": "Manna Center",
                "keywords": ("1 Manna",),
                "info": ({
                    "address": "670 Heritage Ct.", "near": "The intersection of S. Carl and W. Tulsa"
                },),
                "isLeaf": True,
                "phrase": ("1.The Manna Center",),
            },
            {
                "name": "Genesis House",
                "keywords": ("2 Genesis",),
                "info": ({
                    "Phone": "479-549-3438",
                    "Address": "1402 N Inglewood",
                    "Locations": "Across from Dollar General on Cheri Whitlock",
                },),
                "isLeaf": True,
                "phrase": ("2.Genesis House",),
            },
        )
    },
    {
        "name": "communication",
        "keywords": ("address letter mail email e-mail call cell phone telephone",),
        "phrase": ("help with receiving phone calls/mail/email",),
        "options": ("Here are some options:",),
        "children": (
            {
                "name": "Genesis House",
                "keywords": ("1 Genesis",),
                "info": ({
                    "Phone": "479-549-3438",
                    "Services": "Email, Telephone, Physical Mail",
                    "Address": "1402 N Inglewood",
                    "Locations": "Across from Dollar General on Cheri Whitlock",
                },),
                "isLeaf": True,
                "phrase": ("1.Genesis House",),
            },
        )
    },
    {
        "name": "jobs",
        "keywords": ("job work career employ employment employed fired laid",),
        "phrase": ("help finding a job",),
        "options": ("What kind of job help are you looking for?",),
        "children": (
            {
                "name": "Genesis House",
                "keywords": ("1 Genesis",),
                "info": ({
                    "Phone": "479-549-3438",
                    "Address": "1402 N Inglewood",
                    "Locations": "Across from Dollar General on Cheri Whitlock",
                },),
                "isLeaf": True,
                "phrase": ("1.Genesis House",),
            },
            {
                "name": "Goodwill",
                "keywords": ("2 Goodwill",),
                "info": ({
                     "Phone": "479-373-6137",
                     "Address": "1001 S Mt Olive St",
                     "Location": "Corner of 412 & Mt Olive",
                },),
                "isLeaf": True,
                "phrase": ("2.Goodwill",),
            },
        )
    },
    {
        "name": "pregnancy",
        "keywords": ("pregnant pregnancy prenatal expecting baby mother",),
        "phrase": ("help with a pregnancy",),
        "options": ("Here are some pregnancy resources:",),
        "children": (
            {
                "name": "Choices Pregnancy",
                "keywords": ("1 Choices",),
                "info": ({
                    "Phone": "479-549-3322",
                    "Hours": "10-5 M-W, 10-7 Th, 10-2 F",
                    "Services": "Free testing/ultrasound, clothing",
                    "Address": "608 S. Hico St.",
                },),
                "isLeaf": True,
                "phrase": ("1.Choices Pregnancy",),
            },
            {
                "name": "Community Clinic",
                "keywords": ("2 Community",),
                "info": ({
                    "Provides": "Early prenatal care, Medicaid signup",
                    "Phone": "855-438-2280",
                    "Address": "500 S Mt Olive",
                    "Location": "Across from library",
                },),
                "isLeaf": True,
                "phrase": ("2.Community Clinic",),
            },
            {
                "name": "WIC",
                "keywords": ("3 WIC",),
                "info": ({
                    "Phone": "479-549-3790",
                    "Services": "Food, formula",
                    "Location": "Downtown Siloam",
                    "Address": "101 W University St",
                },),
                "isLeaf": True,
                "phrase": ("3.WIC",),
            }
        )
    },
    {
        "name": "senior citizen",
        "keywords": ("senior elder elderly old grandmother grandfather",),
        "phrase": ("senior help",),
        "options": ("Which senior help do you want info about?",),
        "children": (
            {
                "name": "Siloam Senior Center",
                "keywords": ("1 Center",),
                "info": ({
                    "phone": "479-524-5735",
                    "address": "750 Heritage Ct.",
                    "near": "Across from the Post Office",
                },),
                "isLeaf": True,
                "phrase": ("1.Senior Center",),
            },
            {
                "name": "Kind at Heart Ministries",
                "keywords": ("2 Kind Heart",),
                "info": ({
                    "Phone": "479-373-6281",
                    "Hours": "1-5 Mon-Fri",
                    "Services": "Help with house accessibility, work & visits for seniors 70+",
                    "Locations": "18561 Meadowview",
                },),
                "isLeaf": True,
                "phrase": ("2.Kind at Heart",),
            },
        )
    },
    {
        "name": "legal resources",
        "keywords": ("legal lawyer",),
        "phrase": ("help with legal services",),
        "options": ("Which legal service do you want info about?",),
        "children": (
            {
                "name": "Arkansas Legal Services",
                "keywords": ("1 Services",),
                "info": ({
                    "Phone": "800-952-9243",
                    "Website": "https://bit.ly/2Wj1L1p",
                },),
                "isLeaf": True,
                "phrase": ("1.Arkansas Legal Services",),
            },
            {
                "name": "Arkansas Legal Aid",
                "keywords": ("2 Aid",),
                "info": ({
                    "Phone": "870-972-9224",
                    "Website": "https://arlegalaid.org/",
                },),
                "isLeaf": True,
                "phrase": ("2.Arkansas Legal Aid",),
            },
        )
    },
    {
        "name": "immigration resources",
        "keywords": ("immigration immigrant immigrate undocumented deported deportation deport refugee ice",),
        "phrase": ("help with immigration resources",),
        "options": ("What kind of resources are you looking for?",),
        "children": (
            {
                "name": "general",
                "keywords": ("general",),
                "phrase": ("general immigration resources",),
                "children": [
                    {
                        "name": "Catholic Charities",
                        "keywords": ("1 Catholic Charities",),
                        "info": ({
                            "Phone": "479-927-1996",
                            "Address": "2022 W. Sunset Ave., Springdale",
                            "Hours": "Walk-in consultations every Tues 8am",
                            "Website": "https://bit.ly/2WiPBow",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Catholic Charities",),
                    },
                    {
                        "name": "Refugee Center Online",
                        "keywords": ("2 Refugee Center Online",),
                        "info": ({
                            "Website": "https://bit.ly/2okbj9T",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Refugee Center Online",),
                    },
                ]
            },
            {
                "name": "legal",
                "keywords": ("legal",),
                "phrase": ("immigration legal aid",),
                "children": [
                    {
                        "name": "Arkansas Legal Services",
                        "keywords": ("1 Services",),
                        "info": ({
                            "Phone": "800-952-9243",
                            "Website": "https://bit.ly/2Wj1L1p",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Arkansas Legal Services",),
                    },
                    {
                        "name": "Arkansas Legal Aid",
                        "keywords": ("2 Aid",),
                        "info": ({
                            "Phone": "870-972-9224",
                            "Website": "https://arlegalaid.org/",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Arkansas Legal Aid",),
                    },
                ]
            },
        )
    },
    {
        "name": "mental health",
        "keywords": ("mental behavioral depressed depressing depression sad grief crying anxiety panic insane crazy nuts nervous nerve breakdown",),
        "phrase": ("a mental health service",),
        "options": ("What kind of service are you needing?",),
        "children": (
            {
                "name": "counselor",
                "keywords": ("counselor behavioral",),
                "phrase": ("a counseling service",),
                "children": [
                    {
                        "name": "JBU Care Clinic",
                        "keywords": ("1 JBU",),
                        "info": ({
                            "phone": "479-524-7300",
                            "address": "2125 W. University Street",
                            "location": "Across from JBU"
                        },),
                        "isLeaf": True,
                        "phrase": ("1.JBU Care Clinic",),
                    },
                    {
                        "name": "Glenhaven Counseling",
                        "keywords": ("2 Glenhaven",),
                        "info": ({
                            "phone": "479-238-3950",
                            "address": "500 S. Broadway",
                            "location": "Across from Library"
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Glenhaven Counseling",),
                    },
                    {
                        "name": "Ozark Guidance",
                        "keywords": ("3 OGC Ozark Guidance",),
                        "info": ({
                            "phone": "479-524-8618",
                            "address": "710 Holly Street",
                            "location": "Across from the Middle School"
                        },),
                        "isLeaf": True,
                        "phrase": ("3.Ozark Guidance",),
                    }
                ]
            },
            {
                "name": "psychiatrist",
                "keywords": ("psychiatry psychiatrist nurse-practitioner",),
                "phrase": ("a psychiatry service",),
                "children": [
                    {
                        "name": "Ozark Guidance",
                        "keywords": ("1 OGC Ozark Guidance",),
                        "info": ({
                            "phone": "479-524-8618",
                            "address": "710 Holly Street",
                            "location": "Across from the Middle School"
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Ozark Guidance",),
                    }
                ]
            },
        )
    },
    {
        "name": "rehabilitation",
        "keywords": ("aa rehab rehabilitation recover recovery addict addiction addicted dependent drug alcohol alcoholic alcoholics beer wine liquor vodka cocaine crack meth methamphetamine marijuana opioid narcotic oxycodone morphine fentanyl heroin",),
        "phrase": ("addiction or rehab help",),
        "children": (
            {
                "name": "Teen Rehab",
                "keywords": ("adolescent teen",),
                "phrase": ("a teen rehab",),
                "children": (
                    {
                        "name": "Decision Point",
                        "keywords": ("1 decision point",),
                        "info": ({
                            "Phone": "479-756-1060",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Decision Point",),
                    },
                )
            },
            {
                "name": "Adult Rehab",
                "keywords": ("adult",),
                "phrase": ("an adult rehab",),
                "children": (
                    {
                        "name": "Decision Point",
                        "keywords": ("1 decision point",),
                        "info": ({
                            "Phone": "479-756-1060",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Decision Point",),
                    },
                    {
                        "name": "Alcohol and Drug Detox",
                        "keywords": ("2 detox",),
                        "info": ({
                            "Phone": "479-715-4957",
                            "Location": "Fayetteville",
                        },),
                        "isLeaf": True,
                        "phrase": ("2.Alcohol and Drug Detox",),
                    },
                )
            },
            {
                "name": "Men Rehab",
                "keywords": ("men",),
                "phrase": ("a men's rehab",),
                "children": (
                    {
                        "name": "Harbor House",
                        "keywords": ("1 decision point",),
                        "info": ({
                            "Phone": "479-785-4083",
                            "Location": "Fort Smith",
                        },),
                        "isLeaf": True,
                        "phrase": ("1.Alcohol and Drug Detox",),
                    },
                )
            }
        )
    },
    {
        "name": "emergency",
        "keywords": (
            "hospital emergency murder murdering murdered overdose overdosed overdosing shoot shooting shot gun kill killing killed wreck crash crashed crashing kidnap kidnapped kidnapping abduct abducted abducting blood bleeding",
        ),
        "phrase": ("emergency help",),
        "children": (
            {
                "name": "911",
                "keywords": ("1 911 emergency",),
                "info": ({
                    "phone": "911",
                    "address": "603 N. Progress Ave",
                    "location": "ER across from Highschool"
                },),
                "isLeaf": True,
                "phrase": ("1.emergency help",),
            },
            {
                "name": "other",
                "keywords": ("2 other",),
                "info": ({
                    "Other options": "try replying 'suicide' or 'abuse'",
                },),
                "isLeaf": True,
                "phrase": ("2.other",),
            },
        )
    },
    {
        "name": "suicide",
        "keywords": ("suicide suicidal pills hang hanging slit slitting slitted cut cutting death",),
        "phrase": ("urgent help with thoughts of suicide or harm",),
        "children": (
            {
                "name": "911",
                "keywords": ("1 911 emergency",),
                "info": ({
                             "phone": "911",
                             "address": "603 N. Progress Ave",
                             "location": "ER across from Highschool"
                         },),
                "isLeaf": True,
                "phrase": ("1.emergency help",),
            },
            {
                "name": "Suicide Lifeline",
                "keywords": ("2 lifeline",),
                "info": ({
                             "phone": "800-273-8255",
                             "hours": "24/7",
                         },),
                "isLeaf": True,
                "phrase": ("2.suicide lifeline",),
            },
            {
                "name": "Crisis Hotline",
                "keywords": ("3 crisis hotline",),
                "info": ({
                             "phone": "888-274-7472",
                             "hours": "M-F 8am-1am, Sat-Sun 2pm-midnight",
                         },),
                "isLeaf": True,
                "phrase": ("3.crisis hotline",),
            },
        )
    },
    {
        "name": "domestic violence",
        "keywords": ("domestic violence violent abuse hit hitting beat beaten beating bruise bruised bruising punch punched punchin grab grabbed",),
        "phrase": ("urgent help with violence at home",),
        "children": (
            {
                "name": "911",
                "keywords": ("1 911 emergency",),
                "info": ({
                    "phone": "911",
                    "address": "603 N. Progress Ave",
                    "location": "ER across from Highschool"
                },),
                "isLeaf": True,
                "phrase": ("1 emergency help",),
            },
            {
                "name": "Domestic Abuse Hotline",
                "keywords": ("2 hotline",),
                "info": ({
                    "phone": "800-799-7233",
                    "hours": "24/7",
                    "website": "www.thehotline.org/"
                },),
                "isLeaf": True,
                "phrase": ("2 violence hotline",),
            },
            {
                "name": "Child Abuse Hotline",
                "keywords": ("3 abuse hotline",),
                "info": ({
                     "phone": "800-482-5964",
                     "hours": "24/7",
                },),
                "isLeaf": True,
                "phrase": ("3 child abuse hotline",),
            },
        )
    },
]
